package corejava;

public class DataTypes {

    public static void main(String[] args) {

        byte number1 = -128;
        byte number2 = 127;

        short number3 = -32_768;
        short number4 = 32_767;

        int number5 = -2_147_483_648;
        int number6 = 2_147_483_647;

        float number8 = 1.222f;

        long number7 = 23254646575l;

        double number9 = 1.33d;

        boolean x = true;

        char s = 's';
        char y = '\u00ae'; //'U+00AE

        System.out.println(y);


    }

}
